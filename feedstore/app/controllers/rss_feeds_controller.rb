# This controller fetches feeds and returns an xml formatted
# according to RSS syntax

class RssFeedsController < ApplicationController
  
  # Takes a tag and return rss feeds
  #
  # The URL formatted as www.ourwebsite.com/rss/tag.rss can
  # be read by an RSS Feed Reader. The request will be responded
  # with an xml formattted in RSS syntax 
  def generate_rss
    #The passed tag
    tag = params[:tag]
    
    # All Feeds with the associated tag
    feeds_urls  = get_feeds_with_tag(tag)
    
    #Fetch the feeds
    feeds_fetcher = FeedsFetcher.new
    @all_feeds = feeds_fetcher.fetch_multiple_feeds(feeds_urls)
  
    #Array of feeds
    @rss_feeds = Array.new
    
    #Put each feed in array
    for item in @all_feeds.keys
          feeds = @all_feeds[item]
          actual_feeds = feeds.entries

          for feed in actual_feeds
              @rss_feeds << feed
          end
     end
    
    # The tag will be title of the tag
    @feed_title = tag
    
    # The xml(rss) builder will generate the response
    respond_to do |format|
         format.rss { render :layout => false }
    end
    
  end
  
  # Get feeds from database with the passed tag
  def get_feeds_with_tag(tag)
      #Retrieve tags  
      tags = Tag.find_all_by_tagname(tag)
      
      feeds = Array.new
      
      for tag in tags
          # Retrieve feeds associated with tags
          feed_item = Feed.find_by_id(tag.feed_id)
          feeds << feed_item
        
      end
      
      #Array for storing feed urls
      feed_urls = Array.new
      
      for item in feeds
        feed_urls << item.url
      
      end
      return feed_urls
  end
end
