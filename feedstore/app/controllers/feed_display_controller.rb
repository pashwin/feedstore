=begin
 This is the all important controller which is responsible for the main view.
 It stitches the Tag and the Feed data together to show it to the view as 
 a single entity. 
=end
class FeedDisplayController < ApplicationController
  
  #The member variables.
  @tag_list = Array.new
  # hash map to save all the names for a given tag
  @tag_vs_name_map = ActiveSupport::OrderedHash.new  
  
  #We would need to perform devise based authentication to 
  #access this controller.
  before_filter :authenticate_user!
  
  #This method is used to retrieve all the names associated with a 
  #particular tag in the Tag table
  private
  def retrieve_names_for_tag(tagname)  
     #TODO: We may not need these methods.
     #Execute and sql query to retrieve names from feed table for the tag in tags table.
     recent_content = Feeds.find_by_sql("select name from feeds where feed_id in (select feed_id from tags where tag="+tagname+")")
     ap recent_content
  end
  
  #This method is used to create all the default variables that 
  #are required for the views to show the content.
  private
  def set_view_variables
    
    @tag_vs_name_map = ActiveSupport::OrderedHash.new    # Need to declare it again for each session 
    @tag_list = Tag.find(:all)                           # call the method to get all tag lists from feed model
    
    for tag in @tag_list
      
        tagname = tag.tagname
      
        #Get feed by id
        feed = Feed.where('id = ?' , tag.feed_id)
     
        feed_name = feed.first.name

        feedname_in_map = @tag_vs_name_map[tagname]
    
        if feedname_in_map.nil?
              feedname_in_map = Array.new
              feedname_in_map << feed_name
        else
              feedname_in_map <<  feed_name
        end
       
        @tag_vs_name_map[tagname] = feedname_in_map
      end
      
    rescue => msg
        ap "Error: " + msg.to_s;   
   
  end

  #This method is used to populate the content associated with 
  #a particular tag and the name. We make the name unique across the
  #tag, so we can get the unique url associated with the tag + name comnination
  public
  def populate_content(tag, name)
    
    #create the mapping
    @url_vs_feed_map = Hash.new
    feeds_urls = []
    
    #Get the URL associated with the name from the feed database.    
    feed = Feed.find_by_name(name)
    feed_url = feed.url
    feeds_urls << feed.url
    
    feeds_fetcher = FeedsFetcher.new
    @url_vs_feed_map = feeds_fetcher.fetch_multiple_feeds(feeds_urls)
    
    ap "Done with feed collection"
    
    return @url_vs_feed_map
  end
  
  #This method is used to get the content for every rss url
  #associated with the a particular tag. This will be a multiple
  #content fetch and populated in the variable.
  public
  def populate_content_for_tag(tag)
     
     #create the main varaible reqiured in the view.
     @url_vs_feed_map = Hash.new
     
     unless tag.nil?
          feeds_urls = []
         feed_names = @tag_vs_name_map[tag]
         for feed_name in feed_names
           feed = Feed.find_by_name(feed_name)
           feed_url = feed.url
           feeds_urls << feed.url
         end 
    
        #For every url, fetch the feeds.
        feeds_fetcher = FeedsFetcher.new
        @url_vs_feed_map = feeds_fetcher.fetch_multiple_feeds(feeds_urls)

     end
     
    
    ap "Done with feed collection"
    return @url_vs_feed_map
  end
      
  #This represents the goto method for the main page feed display
  #It decides what to display based on the parameters passed.
  #It does an AJAX style update when required.
  public
  def index
      
      set_view_variables()
      
      tag =   params[:tag]
      name = params[:name]
      
      #When we are not passed any parameters from the client then
      #it the initial request and we need to display the defatult
      #page with the first tags content.
      if tag.nil? and name.nil?
        
        first_tag = @tag_vs_name_map.keys.first
        @url_vs_feed_map = populate_content_for_tag(first_tag)

        respond_to do |format|
           format.html # index.html.erb
           format.json { render :json => @feeds }
           #format.mobile 
         end
      
      #Now if only the name is passed, then update AJAX style with
      #only that page content.   
      elsif name.nil?
        @url_vs_feed_map = populate_content_for_tag(tag)
      
        respond_to do |format|
          format.js
          format.mobile {render :file =>  "#{Rails.root}/app/views/feed_display/_feedview.mobile.erb"}
        end
      else
        @url_vs_feed_map = populate_content(tag, name)
      
        respond_to do |format|
          format.js
          format.mobile {render :file =>  "#{Rails.root}/app/views/feed_display/_feedview.mobile.erb"}
        end
      end
  end
end