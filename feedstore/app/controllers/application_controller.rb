# Application controller
# Checks if the request is arriving from a desktop browser or
# other devices and sets a param in the session to 
# indicate that.
class ApplicationController < ActionController::Base
  protect_from_forgery
  
  #This method represents the default path, once the user is logged in
  #to our application using devise library. 
  
  before_filter :prepare_for_mobile
  
  
  def after_sign_in_path_for(resource)
    feed_display_index_path
  end



  def mobile_device?
    request.user_agent =~ /Mobile|webOS|iPhone|iPad/
  end

  helper_method :mobile_device?
  
  def prepare_for_mobile
    if mobile_device?
      session[:mobile_param] = "1"
      request.format = :mobile
      
    end
    
  end  
  
  
end
