require 'will_paginate'
#Controller for the page where feeds can be viewed and actions performed
class FeedsController < ApplicationController
  
  @url_vs_tagnames = Hash.new
  
  #We would need to perform devise based authentication to 
  #access this controller.
  before_filter :authenticate_user!
  
  private 
  def set_view_variables
    
    ap "In feeds controller set_view_variables"
    #Create the hashes here
    @url_vs_tagnames = Hash.new
    
    #Get all the unique tags in the tags table
    tags_list = Tag.all
    
    unless tags_list.nil?
      for tagitem in tags_list
        #Get the feed associated with the tag 
        feeditem = Feed.where("id = ?", tagitem.feed_id)
        
        url = feeditem.first.url;
        
        unless url.nil?
          #Put url in the map
          if @url_vs_tagnames[url].nil?
             @url_vs_tagnames[url] = tagitem.tagname
          else
             @url_vs_tagnames[url] = @url_vs_tagnames[url] + ', ' + tagitem.tagname
          end
        end
      end
    end
    
  end
  
  # GET /feeds
  # GET /feeds.json
  # This method fetches all the feeds for display
  public
  def index
    
    set_view_variables()
    
    # Displays feeds with pagination enabled- 10 Feeds are displayed
    # per page. Prev, Next buttons are provided if more than 10
    # feeds are present
    @feeds = Feed.paginate(:page => params[:page], :per_page => 10)
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @feeds }
    end
  end

  # GET /feeds/1
  # GET /feeds/1.json
  # Get individual feed details for display
  def show
    
    set_view_variables()
    
    #Retrieve the feed to be shown
    @feed = Feed.find(params[:id])
    #@new_comment = Comment.new

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @feed }
    end
  end

  # GET /feeds/new
  # GET /feeds/new.json
  # Create new feed and tags for display
  def new
    
    ap params
    
    @feed = Feed.new
    @tag = Tag.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @feed }
    end
  end

  # GET /feeds/1/edit
  # Fetch the feed and the associated tags for editing
  def edit
  
    
    @feed = Feed.find(params[:id])
    
    @tags = Tag.find_all_by_feed_id(@feed.id)
    

  end

  # The tags are seperated by comma. This method splits
  # the tags using comma as delimiter
  def split_comma_seperated_tags(tags_with_comma) 
      split_tags = Array.new
      unless tags_with_comma.nil?
        split_tags = tags_with_comma.split(',')
      end
      
      #Now trim each tag for spaces.
      ret_val = Array.new
      for tag in split_tags
        ret_val.push(tag.strip)
      end
      
      return ret_val
  end
  
  
  # POST /feeds
  # POST /feeds.json
  # Create the feed using the parameters passed from the view
  
  def create
  
    tags = params[:selection]
    
    tags_from_client = split_comma_seperated_tags(tags)
    
    @feed = Feed.new(params[:feed])
    @tags = Array.new
    for each_tag in tags_from_client
        atag = Tag.new
        atag.tagname = each_tag
        @tags << atag
    end
    
    feed  = params[:feed]
    
    url =  feed['url']
    
    
    # If url doesnt have http or https, then attach it
    # Rails inbuilt url validator expects url to start with either http
    # or https
    if url.length > 0
      unless url.match(/^http/)
          @feed.url = "http://"  + url
      end
    end
    
    if validate_tags(tags, @feed)
        
        respond_to do |format|
            format.html { render :action => "new" }
        end
    
    return
    
    end
    
    

    error = false
    begin
      
      
      Feed.transaction do
      
        
        @tags = Array.new
        for each_tag in tags_from_client
          atag = Tag.new
          atag.tagname = each_tag
          @tags << atag
        end
        
        @feed.tags = @tags
        ap @tags
        
        @feed.save!
        
      
    end
      
    rescue Exception => e
      error = true
    end   
       
    respond_to do |format|
      if error == false
        format.html { redirect_to @feed, :notice => 'Feed was successfully created.' }
        format.json { render :json => @feed, :status => :created, :location => @feed }
      else
        format.html { render :action => "new" }
        format.json { render :json => @feed.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def validate_tags(tags, feed) 
  
  error = false
  tags_from_client = Array.new
    begin
      tags_from_client = split_comma_seperated_tags(tags)
    rescue 
      error = true
      feed.errors.add("tag", "  not valid")
    end
    
    if tags_from_client.length == 0
          error = true
          feed.errors.add("tag", "cannot be empty")
    end
    for tag in tags_from_client
        if tag.length == 0
          error = true
          feed.errors.add("tag", " cannot be empty")
        end
    end


  return error
  end

  # PUT /feeds/1
  # PUT /feeds/1.json
  # Updates data for individual feed. Stores the feed 
  # and tags in the database
  def update
  
    @feed = Feed.find(params[:id])
    @tags = Tag.find_all_by_feed_id(@feed.id)
    
    tags = params[:selection]
    
    feed  = params[:feed]
    url =  feed['url']
    
    # If url doesnt have http or https, then attach it
    # Rails inbuilt url validator expects url to start with either http
    # or https
    if url.length > 0
      unless url.match(/^http/)
          @feed.url = "http://"  + url
      end
    end
    if validate_tags(tags, @feed)
        
        respond_to do |format|
            format.html { render :action => "edit" }
        end
    
    return
    
    end
    
    
    tags_from_client = split_comma_seperated_tags(tags)
    
    #Either save everything or dont. Atomic operations.
    @tags = Array.new
    
    Tag.transaction do
      
        Tag.destroy_all(:feed_id => @feed.id)
        
        #Save new tags in the database    
        for tag_name in tags_from_client
            @tag = Tag.new
            @tag.feed_id=@feed.id
            @tag.tagname=tag_name
            @tags << @tag
        end
      
    end
    @feed.tags = @tags
    
    respond_to do |format|
      if @feed.update_attributes(params[:feed])
        format.html { redirect_to @feed, :notice => 'Feed was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @feed.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /feeds/1
  # DELETE /feeds/1.json
  # Delete from database. Deletes both feed and the associated tags using 
  # the belongs_to and has_to association
  def destroy
    @feed = Feed.find(params[:id])
    @feed.destroy

    respond_to do |format|
      format.html { redirect_to feeds_url }
      format.json { head :no_content }
    end
  end
end
