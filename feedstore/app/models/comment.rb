# Model to store comment in database
# A comment belongs to feed
class Comment < ActiveRecord::Base
  attr_accessible :body, :feed_id
  belongs_to :feed
  
  validates_presence_of :body
end
