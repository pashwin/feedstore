# Model to represent the feed entry.
# This model contains 
#  name and url

class Feed < ActiveRecord::Base
  
  #The attributes that are accessible on the feeds db.
  attr_accessible :name,  :url, :userid
  
  has_many :comments , :dependent => :destroy
  has_many :tags , :dependent => :destroy 
 
  MIN_NAME_LENGTH = 2
  MAX_NAME_LENGTH = 25
   
  #All fields need to exist while inserting to the database.
  validates :name, :presence => true
  validates :url, :presence => true
  validates :userid, :presence => true
  
  #All the other validations required for each field.
  validates :name, :length => {  
    :minimum   => MIN_NAME_LENGTH,
    :maximum   => MAX_NAME_LENGTH,
    :too_short => "must have at least %{count} characters",
    :too_long  => "must have at most %{count} characters"
  }
  
  #Validate the URL for it being a valid URL in format
  validates :url, :uri => { :format => /(^$)|(^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix }
  
  #Validate the URL for being a valid rss feed.
  validates :url, :rss => true
  
  validates_uniqueness_of :url
  validates_uniqueness_of :name
 
end
