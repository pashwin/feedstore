class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.string :tag
      t.string :name
      t.string :url
      t.string :userid

      t.timestamps
    end
  end
end
