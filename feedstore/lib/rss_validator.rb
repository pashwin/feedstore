require 'feedzirra'
#
# This method is used to validate a given url as a rss feed.
# It sends a request for the feed and based on the content decides if
# the url is a valid rss feed. 
# 
class RssValidator < ActiveModel::EachValidator
  
  def validate_each(record, attribute, value)
    
    if value.nil?
      record.errors[attribute] << "is not a valid rss feed"
    else
      
      
      feedResponse = FeedsFetcher.new.fetch_single_feed(value)
      
      #Check for error. If the feed is empty or if the feed 
      #contained a HTTP error response code.
      if feedResponse.nil?
        record.errors[attribute] << "is supposed to be a RSS feed"
      end
      
      if feedResponse.class == Fixnum
        record.errors[attribute] << "feed returned a error. " + feedResponse.to_s
      end    
      
    end
  end

end