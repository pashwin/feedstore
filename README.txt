Readme for  "FeedStore"

* FeedStore is an aggregator of "feeds". 
* Users can add feed URLs
* Users can subscribe to feed-groups which automatically subscribes them to all feeds within a group.
*  Has a seperate view for iPad


To Run this application:

1. Unzip the folder feedstore and save it a local directory.

2. Migrate the database -- Run rake db:migrate to make sure that database tables are populated as expected.

3. Start the rails server by issuing command in Terminal : rails server (You may have to run the bundle install for this to work)

4. In Browser enter the address 0.0.0.0:3000

5. A welcome page is shown, with a form for Sign up or Sign in.

6. If you are using the application first time, click on Sign Up link, it will show a Sign up page with a form to enter email, password and password confirmation. Once you enter all the fields and click on Sign up button it will redirect to the feed display page. Initially there will be no data as there are no feeds in the database. If you would like to create the database entries that we have prepopulated, you could run the `rake db:seed` command which would add interesting feeds to the feeds table. 

7. To enter the feeds click on Manage feeds link. This is the database view of our application. It will list all the feeds present in database. To enter a new feed click on New Feed link. It will show a form to enter a new feed. User should Enter tags, feed name, feed url and click on Create feed. User Name is populated by deafult and cannot be altered. Before saving the data in the database validations will be performed. 

8. Once a feed is created, user can Add comments. Edit, Delete and Show operations are supported on feeds and comments. 

9. Home page will display the content of rss feeds. In the left pane all the feeds will by categorized by their tags. Once user clicks on any category/tag all the feed names associated with that tag will be displayed. Clicking on a feed name will show its content in right pane. By default when a user selects a tag,the content pane will show the aggregated content of the feeds.

10. In content view user can see the actual hyperlink for the feed data, Summary from the feed, and the published time of that feed. Clinking on the link will redirect user to the actual page. 

 
11. Every tag has its associated RSS feed. Clicking on the RSS icon will give the Rss feed for that tag which can be read by any RSS reader. 



On IPAD

1. In the welcome page user has to sign up or sign in.

2. Once the user has logged in to the application all the tags and their associated names will be shown.

3. Clicking on any name will redirect to another page which will display the actual content of that feed.